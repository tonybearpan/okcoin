require 'securerandom'
require 'rest-client'
# require File.dirname(__FILE__) + '/key'
$LOAD_PATH.unshift(File.dirname(__FILE__))
require 'key'

def sign(key_str)
	return Digest::MD5.hexdigest(key_str).upcase
end

def history(symbol,page,len,status)
	url = 'https://www.okcoin.cn/api/v1/order_history.do'
	str = "api_key=#{$ak}&current_page=#{page}&page_length=#{len}&status=#{status}&symbol=#{symbol}&secret_key=#{$sk}"
	history = RestClient.post url, 
		:api_key =>"#{$ak}", 
		:current_page => "#{page}", 
		:page_length =>"#{len}", 
		:status => "#{status}", 
		:symbol => "#{symbol}", 
		:sign => "#{sign(str)}"
	return history
=begin
	请求参数
	参数名	描述
	api_key
	用户申请的apiKey
	symbol
	btc_cny: 比特币 ltc_cny: 莱特币
	status
	查询状态 0：未完成的订单 1：已经完成的订单 （最近七天的数据）
	current_page
	当前页数
	page_length
	每页数据条数，最多不超过200
	sign
	请求参数的签名
=end
end

def info()
	url = 'https://www.okcoin.cn/api/v1/userinfo.do'
	str = "api_key=#{$ak}&secret_key=#{$sk}"
	return RestClient.post url, :api_key => "#{$ak}", :sign => "#{sign(str)}"
end

def trade(symbol, type, price, amount)
=begin
	URL https://www.okcoin.cn/api/v1/trade.do
	示例
	# Request 
	POST https://www.okcoin.cn/api/v1/trade.do
	# Response
	{"result":true,"order_id":123456}
	返回值说明
	result:true代表成功返回
	order_id:订单ID
	请求参数
	参数名	描述
	api_key
	用户申请的apiKey
	symbol
	btc_cny: 比特币 ltc_cny: 莱特币
	type
	买卖类型： 限价单（buy/sell） 市价单（buy_market/sell_market）
	price
	下单价格 [限价买单(必填)： 大于等于0，小于等于1000000 | 市价买单(必填)： BTC :最少买入0.01个BTC 的金额(金额>0.01*卖一价) / LTC :最少买入0.1个LTC 的金额(金额>0.1*卖一价)]（市价卖单不传price）
	amount
	交易数量 [限价卖单（必填）：BTC 数量大于等于0.01 / LTC 数量大于等于0.1 | 市价卖单（必填）： BTC :最少卖出数量大于等于0.01 / LTC :最少卖出数量大于等于0.1]（市价买单不传amount）
	sign
	请求参数的签名
=end
	url = 'https://www.okcoin.cn/api/v1/trade.do'
	str = "amount=#{amount}&api_key=#{$ak}&price=#{price}&symbol=#{symbol}&type=#{type}&secret_key=#{$sk}"
	puts RestClient.post url, :api_key => "#{$ak}", :sign => "#{sign(str)}", :symbol => "#{symbol}", :type => "#{type}", :price => "#{price}", :amount => "#{amount}"
	puts "成功#{type}#{symbol}#{amount}个，成交$价格#{price}元"
end

def sell_market(symbol, type, amount)
	url = 'https://www.okcoin.cn/api/v1/trade.do'
	str = "amount=#{amount}&api_key=#{$ak}&symbol=#{symbol}&type=#{type}&secret_key=#{$sk}"
	puts RestClient.post url, :api_key => "#{$ak}", :sign => "#{sign(str)}", :symbol => "#{symbol}", :type => "#{type}", :amount => "#{amount}"
	puts "成功#{type}#{symbol}#{amount}个"
end

def buy_market(symbol, type, price)
	url = 'https://www.okcoin.cn/api/v1/trade.do'
	str = "api_key=#{$ak}&price=#{price}&symbol=#{symbol}&type=#{type}&secret_key=#{$sk}"
	puts RestClient.post url, :api_key => "#{$ak}", :sign => "#{sign(str)}", :symbol => "#{symbol}", :type => "#{type}", :price => "#{price}"
	puts "成功#{type}#{symbol}个，成交$价格#{price}元"
end

def cancle(symbol,order_id)
=begin
	URL https://www.okcoin.cn/api/v1/cancel_order.do
	示例
	# Request 
	POST https://www.okcoin.cn/api/v1/cancel_order.do
	# Response
	#多笔订单返回结果(成功订单ID,失败订单ID)
	{"success":"123456,123457","error":"123458,123459"}
	返回值说明
	result:true撤单请求成功，等待系统执行撤单；false撤单失败(用于单笔订单)
	order_id:订单ID(用于单笔订单)
	success:撤单请求成功的订单ID，等待系统执行撤单(用于多笔订单)
	error:撤单请求失败的订单ID(用户多笔订单)
	请求参数
	参数名	描述
	api_key
	用户申请的apiKey
	symbol
	btc_cny: 比特币 ltc_cny: 莱特币
	order_id
	订单ID(多个订单ID中间以","分隔,一次最多允许撤消3个订单)
	sign
	请求参数的签名
=end
	url = 'https://www.okcoin.cn/api/v1/cancel_order.do'
	str = "api_key=#{$ak}&order_id=#{order_id}&symbol=#{symbol}&secret_key=#{$sk}"
	rc = RestClient.post url, :api_key => "#{$ak}", :sign => "#{sign(str)}", :symbol => "#{symbol}", :order_id => "#{order_id}"
	p rc.args
	puts "成功取消#{symbol}订单：#{order_id}￥￥￥"
end

def json(arg)
    return JSON.parse(arg)
end

puts '============Info>>>>>>>>>>>>>>>>>>'
p info()	
puts '>>>>>>>>>>>>History=================='
p history('btc_cny',1,1,1)
puts '=================End=================='